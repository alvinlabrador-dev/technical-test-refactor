// config
const API_URL = "http://localhost:8888/images";

async function fetchImages(category, page = 1) {
  const reqArr = [];
  const maxPage = page * 3; // 6
  const pageStart = page > 1 ? page + 2 : page; // 4
  for (let i = pageStart; i <= maxPage; i++) {
    reqArr.push(
      fetch(`${API_URL}?category=${category}&page=${i}`).then(
        async (res) => await res.json()
      )
    );
  }
  const images = [];
  await Promise.all(reqArr).then(async (res) => {
    images.push(...[].concat(...res));
  });
  return images;
}

export default fetchImages;
