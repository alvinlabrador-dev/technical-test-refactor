import fetchImages from "./ImageDataGetter";

class ImageGridViewRenderer {
  imageCount = 1;
  menuWrapper = null;
  imagesWrapper = null;
  paginationWrapper = null;
  loading = false;

  constructor(rootEl, categories, activeCategory, page) {
    this.rootEl = rootEl;
    this.categories = categories;
    this.activeCategory = activeCategory;
    this.page = page;
  }
  createLinks() {
    const links = [];
    this.categories.map((item) => {
      const currentEl = `<span class="sr-only">(current)</span>`;
      const isActive = item.href === this.activeCategory;
      const linkText = isActive ? `${item.text} ${currentEl}` : item.text;
      const linkClass = isActive ? "active" : "";

      const link = `<a href="#" data-category="${item.href}" class="nav-link ${linkClass}">${linkText}</a>`;
      links.push(link);
    });
    return links.join("");
  }
  renderNav() {
    const navEl = document.createElement("div");
    navEl.innerHTML = `
      <div class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Photo Sharing App</a>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            ${this.createLinks()}
          </div>
        </div>
      </div>
    `;

    const links = navEl.querySelectorAll(".nav-link");
    links.forEach((link) => {
      const category = link.getAttribute("data-category");
      // const clickCategory = this.changeCategory.bind(this, category)
      link.addEventListener(
        "click",
        this.updateCategory.bind(this, category, 1, null)
      );
    });

    this.menuWrapper.innerHTML = "";

    this.menuWrapper.append(navEl);
  }
  updateCategory(category, page, updatePage, e) {
    e.preventDefault();

    const nextPage =
      updatePage === "increase"
        ? ++page
        : updatePage === "decrease"
        ? --page
        : page;

    // set category and page
    this.activeCategory = category;
    this.page = nextPage;

    if (this.loading) return;

    // rerender nav
    this.renderNav();

    // update history state
    this.changeUrlString(category, page);

    // rerender grid
    this.renderGrid();
  }
  createImageSection(name, url) {
    return `
    <div class="col" style="height: 400px; padding: 10px">
      <img
        class="image"
        src="${url}"
        alt="${name}"
        style="height: 100%; object-fit: cover; width: 100%"
        crossorigin="anonymous"
      />
      <div class="middle">
        <a
          class="btn btn-dark"
          href="${url}"
          download="${name}"
          >DOWNLOAD</a
        >
      </div>
    </div>
    `;
  }
  async renderGrid() {
    // scroll top
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });

    // loader
    const loader = document.querySelector(".loading-overlay");
    this.loading = true;

    // show loader
    loader.style = `display: block`;

    const images = await fetchImages(this.activeCategory, this.page);
    const imageSections = [];
    images.map((img) => {
      imageSections.push(this.createImageSection(img.name, img.url));
    });
    this.imageCount = images.length;

    // display images
    this.imagesWrapper.innerHTML = imageSections.join("");

    // hide loader
    loader.style = `display: none`;

    // rerender pagination
    this.renderPagination();

    // enable rerender
    this.loading = false;
  }
  createPaginationLink(prev) {
    const linkText = prev ? "Previous" : "Next";
    const linkClass = prev ? "prev-item" : "next-item";
    const link = `<li class="page-item ${linkClass}"><a href="" class="page-link">${linkText}</a></li>`;

    return link;
  }
  renderPagination() {
    const pagination = document.createElement("div");
    pagination.setAttribute("class", "container");
    const prevLink = this.createPaginationLink(true);
    const nextLink = this.createPaginationLink();
    pagination.innerHTML = `
      <div class="row items-center p-3">
        <ul class="pagination m-auto">
          ${this.page > 1 ? prevLink : ""}
          ${this.page >= 1 && this.imageCount === 9 ? nextLink : ""}
        </ul>
      </div>
      `;
    // append prev and next links
    let currentPage = this.page;
    const category = this.activeCategory;
    const prevItem = pagination.querySelector(".prev-item");
    if (prevItem)
      prevItem.addEventListener(
        "click",
        this.updateCategory.bind(this, category, currentPage, "decrease")
      );
    const nextItem = pagination.querySelector(".next-item");
    if (nextItem)
      nextItem.addEventListener(
        "click",
        this.updateCategory.bind(this, category, currentPage, "increase")
      );

    // display pagination
    this.paginationWrapper.innerHTML = "";
    this.paginationWrapper.append(pagination);

    return pagination;
  }
  changeUrlString(category, page) {
    const fullUrl = window.location.href;
    const search = window.location.search;
    const getHost = search ? fullUrl.split(search)[0] : fullUrl;
    const url = new URL(getHost);
    if (category && page) {
      url.searchParams.set("category", category);
      url.searchParams.set("page", page);
      return window.history.pushState({}, "", url);
    }
    window.history.pushState({}, "", "/");
  }
  render() {
    this.renderNav();
    this.renderGrid();
    this.changeUrlString();
  }
  init() {
    // main wrapper
    const mainWrapper = document.getElementById(this.rootEl);

    /**
     * create wrappers
     */

    // menu
    const menuWrapper = document.createElement("div");
    menuWrapper.id = "menuWrapper";

    // grid container
    const gridContainer = document.createElement("div");
    gridContainer.setAttribute("id", "grid-container");
    gridContainer.setAttribute("class", "container");
    gridContainer.innerHTML = `
      <div class="loading-overlay">
        <div class="spinner-border" role="status">
          <span class="sr-only">Loading...</span>
        </div>
      </div>
      <div id="nature-images" class="row row-cols-3"></div>
    `;
    const imagesWrapper = gridContainer.querySelector("#nature-images");

    // pagination
    const paginationWrapper = document.createElement("div");
    paginationWrapper.setAttribute("class", "container");
    paginationWrapper.setAttribute("id", "pagination-container");

    // append sections
    mainWrapper.append(menuWrapper);
    mainWrapper.append(gridContainer);
    mainWrapper.append(paginationWrapper);

    // pass section wrappers
    this.menuWrapper = menuWrapper;
    this.imagesWrapper = imagesWrapper;
    this.paginationWrapper = paginationWrapper;

    // call render
    this.render();
  }
}

export default ImageGridViewRenderer;
