import ImageGridViewRenderer from "./ImageGridViewRenderer";

const categoryItems = [
  {
    text: "Nature",
    href: "nature",
  },
  {
    text: "Architecture",
    href: "architecture",
  },
  {
    text: "Fashion",
    href: "fashion",
  },
];

document.addEventListener("DOMContentLoaded", function () {
  const view = new ImageGridViewRenderer(
    "main-view",
    categoryItems,
    "nature",
    1
  );
  view.init();
});
